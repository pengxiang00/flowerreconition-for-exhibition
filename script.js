Parse.initialize(
    "7gAwbIlMNZu2K3A48k75uPwRGQPnL5CJ3QRMmTDg",
    "u3mxCA2KMeyu2Egd6v7Rek9QnRR5aKTGJLilMDCA"
  );

Parse.serverURL = 'https://pg-app-ugdod8udrehs2ez32vcv5kzl3c5881.scalabl.cloud/1/';

//const URL = "https://teachablemachine.withgoogle.com/models/KcOZVj4O5/";
const URL ="https://teachablemachine.withgoogle.com/models/_w4EVkrcI/"



let model, webcam, newlabel, canvas, labelContainer, maxPredictions, bestClass="", camera_on = false, image_upload = false;

function useWebcam() {
    camera_on = !camera_on;

    if (camera_on) {
        init();
        document.getElementById("webcam").innerHTML = "Close Webcam";
    }
    else {
        stopWebcam();
        document.getElementById("webcam").innerHTML = "Start Webcam";
    }
}

async function stopWebcam() {
    await webcam.stop();
    document.getElementById("webcam-container").removeChild(webcam.canvas);
    labelContainer.removeChild(newlabel);
}

// Load the image model and setup the webcam
async function init() {

    const modelURL = URL + "model.json";
    const metadataURL = URL + "metadata.json";

    // load the model and metadata
    model = await tmImage.load(modelURL, metadataURL);
    maxPredictions = model.getTotalClasses();

    // Convenience function to setup a webcam
    const flip = true; // whether to flip the webcam
    webcam = new tmImage.Webcam(200, 200, flip); // width, height, flip
    await webcam.setup(); // request access to the webcam
    await webcam.play();
    window.requestAnimationFrame(loop);

    // append element to the DOM
    document.getElementById("webcam-container").appendChild(webcam.canvas);

    newlabel = document.createElement("div");
    labelContainer = document.getElementById("label-container");
    labelContainer.appendChild(newlabel);
}

async function loop() {
    webcam.update(); // update the webcam frame
    await predict(webcam.canvas);
    window.requestAnimationFrame(loop);
}

// run the image through the image model
async function predict(input) {
    //clear the wiki result. too fast, users can not see the wiki result
  //  document.getElementById("wikiresult").innerHTML=""

    // predict can take in an image, video or canvas html element
    const prediction = await model.predict(input);

    var highestVal = 0.00;
    bestClass = "";
    bestPrediction=0;

    result = document.getElementById("label-container");
    for (let i = 0; i < maxPredictions; i++) {
        var classPrediction = prediction[i].probability.toFixed(2);
        if (classPrediction > highestVal) {
            highestVal = classPrediction;
            bestClass = prediction[i].className;
            bestPrediction= classPrediction;
        }
    }

    if (bestClass == "Daisy" || bestClass == "Dandelion" || bestClass == "Sunflower") {
        newlabel.className = "alert alert-warning";
    }
    else {
        newlabel.className = "alert alert-danger";
    }

    //displaly the result only it bestPReiction is more than 0.90
if (bestPrediction >0.94){
  //  newlabel.innerHTML = bestClass + " with confidence:  " + bestPrediction;

  newlabel.innerHTML = bestClass ;
   //display wiki info automatically
   askwiki();
}
  
}

async function getPredictions() {

    canvas = document.createElement("canvas");
    var context = canvas.getContext("2d");
    canvas.width = "224";
    canvas.height = "224";
    context.drawImage(img, 0, 0);
    document.getElementById("uploadedImage").appendChild(canvas);

    newlabel = document.createElement("div");
    labelContainer = document.getElementById("label-container-cam");
    labelContainer.appendChild(newlabel);

    await predict(canvas);
}

function askwiki(){
 // alert(bestClass);
  switch(bestClass)
  {
    case "No Flower":
        wikiResult = ""  
        break;
    case "Rose Red" :
        wikiResult ="A rose is a woody perennial flowering plant of the genus Rosa, in the family Rosaceae, or the flower it bears. There are over three hundred species and tens of thousands of cultivars.[citation needed] They form a group of plants that can be erect shrubs, climbing, or trailing, with stems that are often armed with sharp prickles.[citation needed] Their flowers vary in size and shape and are usually large and showy, in colours ranging from white through yellows and reds. Most species are native to Asia, with smaller numbers native to Europe, North America, and northwestern Africa.[citation needed] Species, cultivars and hybrids are all widely grown for their beauty and often are fragrant. Roses have acquired cultural significance in many societies. Rose plants range in size from compact, miniature roses, to climbers that can reach seven meters in height.[citation needed] Different species hybridize easily, and this has been used in the development of the wide range of garden roses."
        break;
      case "Striped Lily":
        wikiResult ="The flowers, generally 2–4, are smaller than other members of the genus. The paraperigon features bristles at the throat of the tepal tube. The perigone is about 7.6–10 cm in size and the tepal segments are 2–2.5 cm broad in their middle. Their colour is a bright red with a green keel that extends halfway up the segment. The stigma is trifid. [5]"
        break;
      case "Baby's breath":
        wikiResult = "Gypsophila /dʒɪpˈsɒfɪlə/[2][3] is a genus of flowering plants in the carnation family, Caryophyllaceae. They are native to Eurasia, Africa, Australia, and the Pacific Islands.[4] Turkey has a particularly high diversity of Gypsophila taxa, with about 35 endemic species.[5] Some Gypsophila are introduced species in other regions.[4] " +
        "The genus name is from the Greek gypsos  and philios (loving), a reference to the gypsum-rich substrates on which some species grow.[4] Plants of the genus are known commonly as baby's-breath, or babe's breath,[4][6] a name which also refers specifically to the well known ornamental species Gypsophila paniculata."
       break;

       case "West Indian Lantana":
           wikiResult= "Lantana camara (common lantana) is a species of flowering plant within the verbena family (Verbenaceae), native to the American tropics. It is a very adaptable species, which can inhabit a wide variety of ecosystems; once it has been introduced into a habitat it spreads rapidly; between 45ºN and 45ºS and more than 1400m in altitude. It has spread from its native Central and South America to around"
      break;
           case "Celosia":
            wikiResult  = "Celosia argentea, commonly known as the plumed cockscomb or silver cock's comb, is a herbaceous plant of tropical origin, and is known for its very bright colors. In India and China it is known as a troublesome weed."
          break;  

          case "Viola" :
           wikiResult="Viola cornuta, known as horned pansy or horned violet, is a species of flowering plant in the violet family Violaceae, native to the Pyrenees and the Cordillera Cantábrica of northern Spain at an altitude of 1,000–2,300 metres (3,300–7,500 ft)" 
           break;

           case "Marigold":
               wikiResult = "Good flower for bedding and borders of	gardens.Less day length	sensitivity makes it a programmable and easy-to grow for " +	
               "early spring. Naturally dwarf and ranching plants	are ideal for large packs and small pots,and fit perfectly on racks	with little-to-no PGRs required.";
               break;
               
       default :
        wikiResult =   "A flower, sometimes known as a bloom or blossom, is the reproductive structure "+
        "found in flowering plants (plants of the division Angiospermae)" + 
        "The biological function of a flower is to facilitate reproduction, usually by providing" + 
        "a mechanism for the union of sperm with eggs. Flowers may facilitate outcrossing" + 
        "(fusion of sperm and eggs from different individuals in a population) resulting from " + 
        "cross-pollination or allow selfing (fusion of sperm and egg from the same flower) when self-pollination occurs."
          
  }
  var img = document.createElement("img");
  var div = document.getElementById("flowerPicture");
  if (bestClass != "No Flower") {
    
        pictureName = "images/wikiImages/" + bestClass +".png"
        //alert(pictureName)
        //create image
        
        img.src = "Images/wikiImages/" + bestClass + ".jpg";
        img.setAttribute("style", "width: 200px; height: 200px;  border-radius: 50%;");

        
        div.innerHTML=""
        div.appendChild(img);
        div.setAttribute("style", "text-align:center");

        document.getElementById("flowerName").innerHTML =bestClass 
        document.getElementById("wikiresult").innerHTML ="<b>" + bestClass + " </b>" + wikiResult
       // alert("speak");
       // speakText(bestClass);


   	var url=location.href;
        location.href = "#flowerName";
        history.replaceState(null,null,url);
  }else{

    div.innerHTML=""
    document.getElementById("flowerName").innerHTML =""
    document.getElementById("wikiresult").innerHTML =""
  }
}

$(document).ready(function () {
    //fileupload function is disabled
    $("#loadBtn1").on("click", async function () {

        labelContainer = document.getElementById("label-container-cam");

        image_upload = !image_upload;

        if (!image_upload) {
            labelContainer.removeChild(newlabel);
            document.getElementById("uploadedImage").removeChild(canvas);
        }

        const fileUploadControl = $("#fruitimg")[0];
        if (fileUploadControl.files.length > 0) {

            const modelURL = URL + "model.json";
            const metadataURL = URL + "metadata.json";

            // load the model and metadata
            model = await tmImage.load(modelURL, metadataURL);
            maxPredictions = model.getTotalClasses();

            const file = fileUploadControl.files[0];

            const name = "photo.jpg";
            const parseFile = new Parse.File(name, file);

            parseFile.save().then(async function () {
                //The file has been saved to the Parse server

                img = new Image(224, 224);
                img.crossOrigin = "Anonymous";
                img.addEventListener("load", getPredictions, false);
                img.src = parseFile.url();

            }, function (error) {
                // The file either could not be read, or could not be saved to Parse.
                result.innerHTML = "Uploading your image failed!";
            });
        }
        else {
            result.innerHTML = "Try Again!";
        }
    });
});