//this is to read out the flower name or description to the user
// in order to enhance the usability
// issues: syncronize the result text and the pronouciation.


function speakText(textToSpeak){ 
    let speaknow = textToSpeak
    responsiveVoice.speak(speaknow, "UK English Male" , {rate: 1.2}, {volume: 1}, {pitch: 2}); 
  } 
  // we set the pitch, rate and volume all in one line with the speak function. They can be written separately. 
  function pause(){ 
    responsiveVoice.pause() 
  } 
  function resume(){ 
    responsiveVoice.resume() 
  } 
  function cancel(){ 
    responsiveVoice.cancel(); 
  }
